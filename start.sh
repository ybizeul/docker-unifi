#!/bin/sh

[ -z $TZ ] || { echo $TZ > /etc/timezone }
java -Xms1024M -Dlog4j.configuration=file:/usr/lib/unifi/data/log4j.properties -jar /usr/lib/unifi/lib/ace.jar start